# dna-mutation




## URL DEMO

- SITE: https://dna.icita.site/
- API REST, DOC: https://dna.icita.site/swagger-ui/index.html


## Technical characteristics

- Live on Instance Digital Ocean Linux centos 
- Deploy in Docker container (check to docker-compose.yml) 
- subdomain by godaddy.com
- Add SSL Certificate with: https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion 

## RUN LOCAL
- required JDK Java 11 install
- requied Maven 
- apply in root command [mvn install]
- apply in root command [java -jar target/dna-mutation-0.1.0.jar --server.port=80]

- other way to run proyect is [mvn spring-boot:run]

## Developer contact

- Name: Ricardo Soler
- Contry: México
- [https://www.linkedin.com/in/risolerh/


## proyect objetive

Project created to anybody that want to see my programing style and good practices. Good Vibes =D

