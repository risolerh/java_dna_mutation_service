create database dna;

create table ope_dna_stats (
	dna varchar(150) PRIMARY KEY,
	has_mutation BOOLEAN NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

commit;
