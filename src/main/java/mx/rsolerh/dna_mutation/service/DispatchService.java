package mx.rsolerh.dna_mutation.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.rsolerh.dna_mutation.model.DnaMutationDto;
import mx.rsolerh.dna_mutation.model.DnaStatDto;
import mx.rsolerh.dna_mutation.model.DnaStatsResponse;
import mx.rsolerh.dna_mutation.model.OpeDnaStatsEntity;
import mx.rsolerh.dna_mutation.repository.DnaMutationRepository;



/**
 * 
 * @author rsole
 *
 */
@Slf4j
@Service
public class DispatchService implements IDispatchService {

	/**
	 * DnaMutationService
	 */
	@Autowired
	private DnaMutationService dnaMutationServ;
	
	/**
	 * DnaMutationRepository
	 */
	@Autowired 
	private DnaMutationRepository dnaMutationRepo;
	
	
	/**
	 * {@inheritDoc}
	 */
	@Cacheable("validDnaSerie")
	@Transactional
	@Override
	public List<DnaMutationDto> validDnaSerie(final List<String> serie) {
		
		List<DnaMutationDto> mutations = dnaMutationServ.validDnaSerie(serie);
		
		String serieString = serie.toString().replace(" ", "");
		OpeDnaStatsEntity entity = new OpeDnaStatsEntity(serieString, !mutations.isEmpty());
		dnaMutationRepo.save(entity);
		log.debug("Entity save...  {}", entity);
		return mutations;
	}

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public DnaStatsResponse getStats() {
		
		DnaStatDto statsReport = dnaMutationRepo.getStats();
		Long noMutations = statsReport.getTotal() - statsReport.getMutations();
		Double ratio = (statsReport.getMutations().doubleValue() * 100 / statsReport.getTotal().doubleValue() );
		
		return DnaStatsResponse.builder()
				.count_no_mutation(noMutations)
				.count_mutations(statsReport.getMutations())
				.ratio(ratio)
				.build();
	}

}
