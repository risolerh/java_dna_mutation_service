package mx.rsolerh.dna_mutation.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.rsolerh.dna_mutation.model.DnaMutationDto;

/**
 * @author rsole
 *
 */
@Slf4j
@Service
@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE, 
	proxyMode=ScopedProxyMode.TARGET_CLASS)
public class DnaMutationService implements IDnaMutationService {

	private final int NUMBER_REAPEATED = 4;
	private List<String> serie;
	private int sizeRows; 
	private List<DnaMutationDto> mutationsFound;
	
	
	/**
	 * Valid dna Series
	 */
	@Override
	public List<DnaMutationDto> validDnaSerie(List<String> serie) {
		
		this.serie = serie;
		this.sizeRows = serie.size();
		this.mutationsFound = new ArrayList<>();
		 
		// iterate row series
		int indexRow = 0;
		for (String row: this.serie) 
		{			
			// validate horizontal letters 
			checkMatches(row, indexRow, 0, "horizontally");			
			// build and validate vertical letters 
			validateVerticallyLine(row, indexRow++, "right vertically");
		}
		
		/// iterate in reverse
		this.serie = serie.stream()
				.map( row -> new StringBuilder(row).reverse().toString() )
				.collect(Collectors.toList());
		
		indexRow = 0;
		
		for (String row: this.serie) 
		{			
			// build and validate vertical letters 
			validateVerticallyLine(row, indexRow++, "left vertically");
		}
		
		return this.mutationsFound;
	}
	
	/**
	 * Validate Vertical Line By row position
	 * 
	 * @param row 
	 * @param indexRow
	 */
	private void validateVerticallyLine(String row, int indexRow, String position) {
		
		// colum iterable top to bottom
		int sizeCols = row.toCharArray().length;
		for (int indexCol = 0; indexCol < sizeCols; indexCol++) 		
		{
			// get vertical line by current row letter
			StringBuilder sb = new StringBuilder();
			putLetterVerticalLoop(indexRow, indexCol, sb);
			
			// valid vertical line 
			log.debug("{} [{}][{}]: {}", position, indexRow, indexCol, sb.toString() );
			checkMatches(sb.toString(), indexRow, indexCol, position);
		}
	}

	
	/**
	 * Recursive function to build letters vertically
	 * 
	 * @param indexRow
	 * @param indexCol
	 * @param sb
	 */
	private void putLetterVerticalLoop( int indexRow, int indexCol, StringBuilder sb) {
		
		// valid if is last row
		if( indexRow > (sizeRows-1)) return;
		
		// get char letters by row
		char[] arrRow = serie.get(indexRow++).toCharArray();		

		// valid if is last letter in  array column		
		if (indexCol > (arrRow.length-1)) return; 
		
		/**** ADD LETTER IN VERTICAL LINE ****/
		sb.append(arrRow[indexCol]);
		
		// loop next letter vertically
		this.putLetterVerticalLoop(indexRow, ++indexCol, sb);
	}
	
	
	
	/**
	 * Check if exist  
	 * 
	 * @param serie
	 */
	private void checkMatches(String line, int indexRow, int indexCol, String position) {
				
        List<String> letterList = new ArrayList<String>(Arrays.asList(line.split("")));
        var sizeRow = letterList.size();
		
		// valid if have correctly  size to check  
		if (sizeRow < NUMBER_REAPEATED ) return;
				
		// discriminate letters line by index
		for (int index = 0; index < sizeRow; index++ ) 
		{
			var lastIndex = index + NUMBER_REAPEATED;
			
			// return if connot be compare
			if (lastIndex > sizeRow) { return; }
			
			// discriminate letters line by index
			List<String> subList = letterList.subList(index, lastIndex);
			
			// valid if is same letter
			if ( subList.stream().distinct().count() == 1) {
				log.debug("!! mutate localizated {}: {} in rowIndex[{}] colIndex[{}]",position, line, indexRow, indexCol);	
				mutationsFound.add( DnaMutationDto.builder()
						.rowIndex(indexRow)
						.colIndex(indexCol)
						.mutation(subList.toString())
						.position(position)
						.build());
			}
		}

	}
	
	
	

}
