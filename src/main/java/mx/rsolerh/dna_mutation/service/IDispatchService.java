
package mx.rsolerh.dna_mutation.service;

import java.util.List;

import mx.rsolerh.dna_mutation.model.DnaMutationDto;
import mx.rsolerh.dna_mutation.model.DnaStatsResponse;

/**
 * @author rsole
 *
 */
public interface IDispatchService {

	/**
	 * Valid Dna Serie
	 * 
	 * @param serie
	 */
	List<DnaMutationDto> validDnaSerie(List<String> serie);
	
	/**
	 * DnaStatsResponse
	 * 
	 * @return
	 */
	DnaStatsResponse getStats();
	
	
}
