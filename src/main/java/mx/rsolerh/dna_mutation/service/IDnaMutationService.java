package mx.rsolerh.dna_mutation.service;

import java.util.List;

import mx.rsolerh.dna_mutation.model.DnaMutationDto;

/**
 * @author rsole
 *
 */
public interface IDnaMutationService {
	
	/**
	 * Valid Dna Serie
	 * 
	 * @param serie
	 */
	List<DnaMutationDto> validDnaSerie(List<String> serie);

}
