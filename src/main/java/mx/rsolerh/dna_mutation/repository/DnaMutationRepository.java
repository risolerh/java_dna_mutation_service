package mx.rsolerh.dna_mutation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mx.rsolerh.dna_mutation.model.DnaStatDto;
import mx.rsolerh.dna_mutation.model.OpeDnaStatsEntity;

@Repository
public interface DnaMutationRepository extends JpaRepository<OpeDnaStatsEntity, String>{

	/**
	 * Get Stats By Dna mutation
	 * 
	 * @return
	 */
	@Query("SELECT new mx.rsolerh.dna_mutation.model.DnaStatDto("
			+ "count(e),"
			+ "sum(case when e.hasMutation = true then 1 else 0 end) )"
			+ " FROM OpeDnaStatsEntity e")
	
	public DnaStatDto getStats();
}
