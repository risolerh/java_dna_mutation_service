package mx.rsolerh.dna_mutation.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author rsole
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class DnaMutationDto implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4654835479658088759L;
	
	private Integer rowIndex;
	private Integer colIndex;
	private String mutation;
	private String position;
	
	
}
