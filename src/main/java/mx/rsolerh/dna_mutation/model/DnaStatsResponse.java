package mx.rsolerh.dna_mutation.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

/**
 * @author rsole
 *
 */
@Builder
@Data
public class DnaStatsResponse implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3843354014656940386L;
	
	
	private Long count_mutations;
	private Long count_no_mutation;
	private Double ratio;
}
