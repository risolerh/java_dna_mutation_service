package mx.rsolerh.dna_mutation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Data
@Table(name="ope_dna_stats")
public class OpeDnaStatsEntity implements Serializable {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3696214651591439114L;
	
	@Id
	@Column(name = "dna")
	private String dna;
	
	@Column(name = "has_mutation")
	private Boolean hasMutation;
	 
}
