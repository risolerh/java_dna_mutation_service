package mx.rsolerh.dna_mutation.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mx.rsolerh.dna_mutation.validation.DnaSerieConstraint;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;


/**
 * @author rsole
 *
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class DnaMutationRequest implements Serializable {

    @NotEmpty(message = "dnaSerie connot be empty...")
    @DnaSerieConstraint
    private List<String> dna;
}
