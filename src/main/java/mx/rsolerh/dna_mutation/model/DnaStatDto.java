package mx.rsolerh.dna_mutation.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author rsole
 *
 */
@Data
@AllArgsConstructor
public class DnaStatDto implements Serializable {
		
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1232972600835441441L;
	
	private Long total;	
	private Long mutations;

	
}
