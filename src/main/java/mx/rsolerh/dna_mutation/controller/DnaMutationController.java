package mx.rsolerh.dna_mutation.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import mx.rsolerh.dna_mutation.model.DnaMutationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import mx.rsolerh.dna_mutation.model.DnaStatsResponse;
import mx.rsolerh.dna_mutation.service.IDispatchService;
import mx.rsolerh.dna_mutation.validation.DnaSerieConstraint;

/**
 * @author rsole
 *
 */
@Slf4j
@RestController()
@Api("DNA Mutation REST")
@RequestMapping( produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_JSON_VALUE })
public class DnaMutationController {
	
	/**
	 * IDnaMutationService
	 */
	@Autowired
	private IDispatchService dispatchServ;
	

	
	@PostMapping("mutation")
	public ResponseEntity<Void> addDnaSerie(
			@RequestBody @Valid DnaMutationRequest dnaSerie) {
		log.debug("Dna recibida: {}", dnaSerie.toString() );
		dispatchServ.validDnaSerie(dnaSerie.getDna());
		return ResponseEntity.status(HttpStatus.OK).build();
	}
	
	/**
	 * Stat 
	 * 
	 * @return
	 */
	@GetMapping("stats")
	public ResponseEntity<DnaStatsResponse> getStats() {
		log.debug("getstats...");
		DnaStatsResponse statsResponse = dispatchServ.getStats();
		return ResponseEntity.status(HttpStatus.OK).body(statsResponse);
	}


}
