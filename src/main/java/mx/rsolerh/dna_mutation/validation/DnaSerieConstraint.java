package mx.rsolerh.dna_mutation.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Documented
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Constraint(validatedBy = DnaSerieConstraintValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface DnaSerieConstraint {

    String message() default "The input list must be 6x6 chains.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
