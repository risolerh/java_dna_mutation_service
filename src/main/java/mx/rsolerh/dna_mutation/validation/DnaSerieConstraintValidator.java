package mx.rsolerh.dna_mutation.validation;

import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author rsole
 *
 */
public class DnaSerieConstraintValidator implements 
	ConstraintValidator<DnaSerieConstraint, List<String>>{

	private final int SIZE_DNA_SERIE = 6;
	
	private final List<Character> VALID_LETTER_DNA = Arrays.asList('A', 'T', 'C', 'G');
		
	@Override
	public boolean isValid(List<String> values, ConstraintValidatorContext context) {
		
		// valid size row
		if (values.size() != SIZE_DNA_SERIE) { return false; }
		
		try {
			// valid colum size and letters
			values.forEach(serie -> validRowSerie(serie) );
		} catch (IllegalArgumentException e) {			
			context.buildConstraintViolationWithTemplate(e.getMessage()).addConstraintViolation();			
			return false;
		}
		return true;
	}
	
	
	/**
	 * @param serie
	 */
	private void validRowSerie(String serie) throws IllegalArgumentException {
		
		char[] characters = serie.toCharArray();
		
		// valid  size in every row
		if (characters == null || characters.length != SIZE_DNA_SERIE) {			
			throw new IllegalArgumentException("Error in size row by dns serie");			
		}
		
		// valid letters
		for (char c : characters) {
			if (!VALID_LETTER_DNA.contains(c)) {
				throw new IllegalArgumentException("Characters no valid in row by dns serie");
			}
		}
	}
	
	

}
