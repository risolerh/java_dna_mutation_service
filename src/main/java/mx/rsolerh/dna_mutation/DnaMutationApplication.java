package mx.rsolerh.dna_mutation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * @author rsole
 *
 */
@EnableCaching
@EnableSwagger2
@SpringBootApplication
public class DnaMutationApplication {

	public static void main(String[] args) {
		SpringApplication.run(DnaMutationApplication.class, args);
	}

}
