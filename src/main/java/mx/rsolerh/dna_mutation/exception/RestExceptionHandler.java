package mx.rsolerh.dna_mutation.exception;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


/**
 * @author rsole
 *
 */
@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 *	handleMethodArgumentNotValid
	 */
	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(    		
    		MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        List<String> errorMessages = new ArrayList<>();
        BindingResult bindingResult = ex.getBindingResult();
        List<ObjectError> errors = bindingResult.getAllErrors();
        for(ObjectError error : errors) {
            String message = error.getDefaultMessage();
            errorMessages.add(message);
        }

        return new ResponseEntity<>((errorMessages), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
    
	
	/**
	 * RuntimeException
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(value = { RuntimeException.class, Exception.class })
	ResponseEntity<Object> handleException(Exception ex) {

	    Map<String, Object> body = new LinkedHashMap<>();
	    body.put("timestamp", LocalDate.now());
	    body.put("status", ex.getLocalizedMessage());
	    body.put("error",ex.getMessage());
	    
	    return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);	  
	}
	
	
	
	/**
	 * ConstraintViolationException
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(value = { ConstraintViolationException.class })
	ResponseEntity<Object> handleException(ConstraintViolationException ex) {

	    Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
	    String errorMessage = "";
	    if (!violations.isEmpty()) {
	        errorMessage = violations.stream().reduce((first, second) -> second).get().getMessage();
	    } else {
	        errorMessage = "ConstraintViolationException occured.";
	    }
	    
	    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);  
	}

}
