package mx.rsolerh.dna_mutation.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import mx.rsolerh.dna_mutation.model.DnaMutationRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import mx.rsolerh.dna_mutation.service.IDispatchService;

import java.util.Arrays;
import java.util.List;


@WebMvcTest(DnaMutationController.class)
class DnaMutationControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private IDispatchService dispatchServ;
	
	ObjectMapper objectMapper = new ObjectMapper();

	@Test
	void testAddDnaSerie() throws Exception {

		List<String> dna = Arrays.asList("ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG");
		DnaMutationRequest request = new DnaMutationRequest(dna);

		mvc.perform(MockMvcRequestBuilders
	  			.post("/mutation")
	  			.contentType(MediaType.APPLICATION_JSON)
	  			.content(objectMapper.writeValueAsString(request))
	  			.accept(MediaType.APPLICATION_JSON)
	  			)
//	      .andDo(print())
	      .andExpect(status().isOk());
	}


	@Test
	void testAddDnaSerieFail() throws Exception {

		List<String> dna = Arrays.asList("ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCZCTA", "TCACTG");
		DnaMutationRequest request = new DnaMutationRequest(dna);

		mvc.perform(MockMvcRequestBuilders
						.post("/mutation")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(request))
						.accept(MediaType.APPLICATION_JSON)
				)
//	      .andDo(print())
				.andExpect(status().is4xxClientError());
	}

}
