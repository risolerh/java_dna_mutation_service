package mx.rsolerh.dna_mutation.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import mx.rsolerh.dna_mutation.model.DnaMutationDto;

@ExtendWith(MockitoExtension.class)
class DnaMutationServiceTest {

	@InjectMocks	
	private DnaMutationService dnaMutationServ;
	
	@Test
	void testValidDnaSerieClean() {
		String[] dna = new String[] {"ATGCGA", "CTGTGC", "TTATGT", "AGAAGG", "CCACTA", "TCACTG"};

		List<DnaMutationDto> mutationsFound = dnaMutationServ.validDnaSerie(Arrays.asList(dna));
		
		assertEquals(mutationsFound.size(),0);
	}
	
	@Test
	void testValidDnaSerieWithMutations() {
		String[] dna = new String[] {"ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"};

		List<DnaMutationDto> mutationsFound = dnaMutationServ.validDnaSerie(Arrays.asList(dna));
		
		assertEquals(mutationsFound.size(),2);
	}

}
